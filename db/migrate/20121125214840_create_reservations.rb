class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.integer :user_id
      t.integer :copy_id
      t.date :reservation_date

      t.timestamps
    end
  end
end
