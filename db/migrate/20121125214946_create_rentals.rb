class CreateRentals < ActiveRecord::Migration
  def change
    create_table :rentals do |t|
      t.integer :user_id
      t.integer :copy_id
      t.date :begin_date
      t.date :end_date
      t.date :last_renewal

      t.timestamps
    end
  end
end
