class CreateAuthors < ActiveRecord::Migration
  def change
    create_table :authors do |t|
      t.string :forename
      t.string :surname
      t.integer :birth_year
      t.integer :death_year

      t.timestamps
    end
  end
end
