class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.integer :author_id
      t.string :title
      t.text :description
      t.integer :publish_year
      t.integer :rental_length

      t.timestamps
    end
  end
end
