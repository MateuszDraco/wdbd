class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :surname
      t.string :forename
      t.string :reference_number
      t.string :email
      t.string :phone
      t.text :address

      t.timestamps
    end
  end
end
