class CreateCopies < ActiveRecord::Migration
  def change
    create_table :copies do |t|
      t.integer :book_id
      t.string :edition_number
      t.integer :publish_year

      t.timestamps
    end
  end
end
