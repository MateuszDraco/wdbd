class CopiesController < ApplicationController
  # GET /copies.json
  # GET /copies
  def index
    @book = Book.find(params[:book_id])
    @copies = @book.copies

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @copies }
    end
  end

  # GET /copies/1
  # GET /copies/1.json
  def show
    @book = Book.find(params[:book_id])
    @copy = @book.copies.find(params[:id])
    @users = User.all

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @copy }
    end
  end

  # GET /copies/new
  # GET /copies/new.json
  def new
    @book = Book.find(params[:book_id])
    @copy = @book.copies.build

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @copy }
    end
  end

  # GET /copies/1/edit
  def edit
    @book = Book.find(params[:book_id])
    @copy = @book.copies.find(params[:id])
  end

  # POST /copies
  # POST /copies.json
  def create
    @book = Book.find(params[:book_id])
    @copy = @book.copies.build(params[:copy])

    respond_to do |format|
      if @copy.save
        format.html { redirect_to book_copies_path(@book), notice: 'Copy was successfully created.' }
        format.json { render json: @book, status: :created, location: @copy }
      else
        format.html { render action: "new" }
        format.json { render json: @copy.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /copies/1
  # PUT /copies/1.json
  def update
    @book = Book.find(params[:book_id])
    @copy = @book.copies.find(params[:id])

    respond_to do |format|
      if @copy.update_attributes(params[:copy])
        format.html { redirect_to book_copies_path(@book), notice: 'Copy was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @copy.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /copies/1
  # DELETE /copies/1.json
  def destroy
    @book = Book.find(params[:book_id])
    @copy = @book.copies.find(params[:id])
    @copy.destroy

    respond_to do |format|
      format.html { redirect_to book_copies_path(@book) }
      format.json { head :no_content }
    end
  end

  def new_rent
    @book = Book.find(params[:book_id])
    @copy = @book.copies.find(params[:id])
    rental = @copy.rentals.build(user_id: params[:user_id], begin_date: Date.today)
    rental.save

    redirect_to [@book, @copy]
  end

  def new_reservation
    @book = Book.find(params[:book_id])
    @copy = @book.copies.find(params[:id])
    reservation = @copy.reservations.build(user_id: params[:user_id], reservation_date: Date.today)
    reservation.save

    redirect_to [@book, @copy]
  end

  def execute_reservation
    @book = Book.find(params[:book_id])
    @copy = @book.copies.find(params[:id])
    reservation = @copy.reservations.find(params[:reservation_id])
    rental = @copy.rentals.build(user_id: reservation.user_id, begin_date: Date.today)
    if rental.save
      reservation.destroy
    end

    redirect_to [@book, @copy]
  end

  def cancel_reservation
    @book = Book.find(params[:book_id])
    @copy = @book.copies.find(params[:id])
    reservation = @copy.reservations.find(params[:reservation_id])
    reservation.destroy

    redirect_to [@book, @copy]
  end

  def renew_rent
    @book = Book.find(params[:book_id])
    @copy = @book.copies.find(params[:id])
    rental = @copy.rentals.find(params[:rental_id])
    rental.last_renewal = Date.today
    rental.save

    redirect_to [@book, @copy]
  end

  def finish_rent
    @book = Book.find(params[:book_id])
    @copy = @book.copies.find(params[:id])
    rental = @copy.rentals.find(params[:rental_id])
    rental.end_date = Date.today
    rental.save

    redirect_to [@book, @copy]
  end
end
