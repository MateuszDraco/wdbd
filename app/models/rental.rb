class Rental < ActiveRecord::Base
  attr_accessible :begin_date, :copy_id, :end_date, :last_renewal, :user_id

  belongs_to :copy
  belongs_to :user

  validates_associated :user, :copy
  validates_presence_of :user, :copy, :begin_date
end
