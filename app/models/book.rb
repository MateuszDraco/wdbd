class Book < ActiveRecord::Base
  attr_accessible :author_id, :description, :publish_year, :rental_length, :title

  belongs_to :author
  has_many :book_categories
  has_many :categories, :through => :book_categories
  has_many :copies

  default_scope order(:title)

  validates_presence_of :title
  validates_associated :author
end
