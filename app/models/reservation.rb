class Reservation < ActiveRecord::Base
  attr_accessible :copy_id, :reservation_date, :user_id

  belongs_to :copy
  belongs_to :user

  validates_associated :user, :copy
  validates_presence_of :user, :copy, :reservation_date
end
