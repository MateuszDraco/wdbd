class User < ActiveRecord::Base
  attr_accessible :address, :email, :forename, :phone, :reference_number, :surname

  has_many :reservations
  has_many :rentals

  validates_presence_of :forename, :surname, :reference_number
  validates_uniqueness_of :reference_number

  default_scope order(:surname, :forename)

  def full_name
    "#{reference_number}: #{surname} #{forename}"
  end
end
