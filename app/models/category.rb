class Category < ActiveRecord::Base
  attr_accessible :name

  has_many :book_categories
  has_many :books, :through => :book_categories

  validates_uniqueness_of :name
  validates_presence_of :name
end
