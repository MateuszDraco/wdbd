class Copy < ActiveRecord::Base
  attr_accessible :book_id, :edition_number, :publish_year

  belongs_to :book
  has_many :rentals
  has_many :reservations

  validates_presence_of :book
  validates_associated :book

  def current_rental
  	day = Date.today
  	rentals.where('begin_date <= ? and end_date >= ?', day, day)
  end
end
