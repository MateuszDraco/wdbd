class Author < ActiveRecord::Base
  attr_accessible :birth_year, :death_year, :forename, :surname

  has_many :books

  scope :by_year, order(:birth_year)
  default_scope order(:surname, :forename)

  def full_name
    "#{forename} #{surname}"
  end
end
