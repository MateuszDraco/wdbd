module CopiesHelper

  def current_state(copy)
  	rental = copy.current_rental.first
  	if rental
  		link_to "#{I18n.t(:rent_until)} #{rental.end_date}", [copy.book, copy]
  	elsif copy.reservations.count > 0
  		link_to "#{current_reservations.cout} #{I18n.t(:reservations)}", [copy.book, copy]
  	else
  		link_to I18n.t(:available), [copy.book, copy]
  	end
  end
end
